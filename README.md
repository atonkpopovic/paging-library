# ANDROID PAGING LIBRARY

## Objectives
- Load multiple repositories from Github based on searc criteria with pagination and caching

## Project structure
- Activity
- List Adapter
- View model
- Repository
- Cache
- Service

## Paging library components
- Paged list
- Paged list Adapter
- Boundary Callback
- Data Source
- DiffUtil

## Benefits
- Load data in chunks
- We can query database and API at different speed (items per page)

## Usage in project 
- Fetch all AB's at once from API but load them in chunks into a list based on category
- Fetch calls from API in sets by 50 items to lover API requests, but load calls in sets of 10 in list for better UX