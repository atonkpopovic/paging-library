package com.kruno.paginglibrary.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.kruno.paginglibrary.api.GithubService
import com.kruno.paginglibrary.data.db.GithubLocalCache
import com.kruno.paginglibrary.data.db.RepoDatabase
import com.kruno.paginglibrary.data.repository.GithubRepository
import com.kruno.paginglibrary.data.viewmodel.ViewModelFactory
import java.util.concurrent.Executors

object Injection {

    private fun provideCache(context: Context): GithubLocalCache {
        val database = RepoDatabase.getInstance(context)
        return GithubLocalCache(database.reposDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideGithubRepository(context: Context): GithubRepository {
        return GithubRepository(GithubService.create(), provideCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithubRepository(context))
    }
}